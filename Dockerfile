# Set the base image to Python 3.9
FROM python:3-alpine

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory to /srv/app
WORKDIR /srv/app

# Set the UID and GID to 1000
ARG USER_ID=1000
ARG GROUP_ID=1000
RUN addgroup -g ${GROUP_ID} pythoniste && \
    adduser -u ${USER_ID} -G pythoniste -s /bin/sh -D pythoniste

# Change ownership of the working directory to the pythoniste user
RUN chown pythoniste:pythoniste /srv/app

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev

RUN pip install --upgrade pip

RUN apk del build-deps

COPY . /srv/app

USER pythoniste