class CommandInterface:
  def execute(self, args):
    raise NotImplementedError("Please Implement this method")
