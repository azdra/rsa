# Projet RSA Encryption

Ce TP implémente l'algorithme de cryptage RSA en Python. RSA est un système de cryptage à clé publique largement utilisé qui permet une communication sécurisée sur un canal non sécurisé.

## Table des matières

- [Projet RSA Encryption](#projet-rsa-encryption)
  - [Table des matières](#table-des-matières)
  - [Introduction](#introduction)
  - [Prérequis](#prérequis)
  - [Installation](#installation)
      - [1. Clonez le projet depuis le dépôt GitLab :](#1-clonez-le-projet-depuis-le-dépôt-gitlab-)
      - [2. Installation de l'environnement de développement](#2-installation-de-lenvironnement-de-développement)
  - [Utilisation](#utilisation)
  - [Auteurs](#auteurs)
  - [Licence](#licence)


## Introduction

Le fonctionnement du cryptage RSA est basé sur la difficulté de factoriser de grands entiers.

On utilise deux nombres premiers p et q. Puis on calcule n = p*q.
Prendre un nombre e qui n'a aucun facteur en commun avec (p-1)(q-1).
Calculer d tel que ed modulo (p-1)(q-1) = 1
Le couple (n,e) constitue la clé publique utilisée pour le chiffrement. Le couple (n,d) est la clé privée pour le déchiffrement.

## Prérequis

Avant d'installer et d'utiliser ce projet, assurez-vous d'avoir les éléments suivants :

- Docker - https://www.docker.com/

## Installation

Suivez les étapes ci-dessous pour installer et configurer le projet :

#### 1. Clonez le projet depuis le dépôt GitLab :

```shell
$ git clone https://gitlab.com/azdra/rsa.git

$ cd rsa
```

#### 2. Installation de l'environnement de développement

```shell
$ sudo chmod +x ./enter.sh
$ sudo chmod +x bin/crypt
$ sudo chmod +x bin/decrypt
$ sudo chmod +x bin/help
$ sudo chmod +x bin/keygen

# Compilation des images docker
$ docker compose build
```

## Utilisation

Pour lancer l'application, exécutez la commande suivante :

```shell
$ bin/crypt --m|message message
```

```shell
$ bin/decrypt --m|message message
```

```shell
$ bin/keygen --s|size key_size
```

```shell
$ bin/help
```

## Auteurs

Ce projet a été développé par [Déb](https://gitlab.com/deb-brunier) & [Baptiste](https://gitlab.com/azdra).

## Licence

Ce projet est sous licence [MIT](LICENSE). Vous pouvez consulter le fichier [LICENSE](LICENSE) pour plus de détails.

