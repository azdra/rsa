import random
import math


def is_prime(number):
  """
  Check if a number is prime.
  Args:
      number (int): The number to check.
  Returns:
      bool: True if the number is prime, False otherwise.
  """
  if number <= 1:
    return False
  if number <= 3:
    return True

  if number % 2 == 0 or number % 3 == 0:
    return False

  i = 5
  while i * i <= number:
    if number % i == 0 or number % (i + 2) == 0:
      return False
    i += 6

  return True


def generate_prime_number(size):
  """
  Generate a prime number of the specified size.
  Args:
      size (int): The size of the prime number to generate.
  Returns:
      int: A random prime number between min_value and max_value (inclusive).
  """

  while True:
    num = random.randint(10 ** (size - 1), 10 ** size - 1)
    if is_prime(num):
      return num


def mod_inverse(e, phi):
  """
  Calculates the modular inverse of e modulo phi.
  Args:
      e (int): An integer representing the base.
      phi (int): An integer representing the modulus.

  Returns:
      int: The modular inverse of e modulo phi.
  """
  for d in range(3, phi):
    if (d * e) % phi == 1:
      return d
  raise ValueError('No mod_inverse for {} and {}'.format(e, phi))


def generate_keypair(bits):
  """
  Generate a pair of public and private keys of the specified size.
  Args:
      bits (int): The size of the key in bits.
  Returns:
      tuple: A tuple of the public and private keys.
  """
  p, q = generate_prime_number(bits), generate_prime_number(bits)
  n = p * q
  phi = (p - 1) * (q - 1)

  e = random.randint(3, phi - 1)  # Common choice for the public exponent

  # Make sure that e and phi are coprime, generate a new one otherwise
  while math.gcd(e, phi) != 1:
    # print(math.gcd(e, phi))
    e = random.randint(3, phi - 1)

  d = mod_inverse(e, phi)

  # public, private
  return (n, e), (n, d)
